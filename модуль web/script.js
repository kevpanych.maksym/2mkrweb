
var film = {
    id: 1,
    назва: "fast and furious 10",
    режисер: "gricenko victor",
    "URL-трейлеру": "https://www.youtube.com/watch?v=123456789",
    "рік випуску": 2023,
    "касові збори": 1000000000,

   
    toString() {
        return `${this.id}. ${this.назва} (${this["рік випуску"]})`;
    }
};


var filmCollection = {
    фільми: [],
    
    
    constructor() {
        this.фільми.push(film);
    },

    
    editFilm(id, newData) {
        const film = this.фільми.find(film => film.id === id);
        if (film) {
            Object.assign(film, newData);
        }
    }
};
filmCollection.constructor();


var editForm = document.getElementById("editForm");
var movieIdInput = document.getElementById("movieId");
var movieTitleInput = document.getElementById("movieTitle");
var movieDirectorInput = document.getElementById("movieDirector");
var movieTrailerInput = document.getElementById("movieTrailer");
var movieYearInput = document.getElementById("movieYear");
var movieRevenueInput = document.getElementById("movieRevenue");


movieIdInput.value = film.id;
movieTitleInput.value = film.назва;
movieDirectorInput.value = film.режисер;
movieTrailerInput.value = film["URL-трейлеру"];
movieYearInput.value = film["рік випуску"];
movieRevenueInput.value = film["касові збори"];


editForm.addEventListener("submit", function(event) {
    event.preventDefault();

    var newData = {
        назва: movieTitleInput.value,
        режисер: movieDirectorInput.value,
        "URL-трейлеру": movieTrailerInput.value,
        "рік випуску": parseInt(movieYearInput.value),
        "касові збори": parseFloat(movieRevenueInput.value)
    };

    filmCollection.editFilm(film.id, newData);
    console.log(film); 
});
